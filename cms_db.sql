-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 04, 2019 at 03:43 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

DROP TABLE IF EXISTS `categorys`;
CREATE TABLE IF NOT EXISTS `categorys` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_show_title` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` double(8,2) DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `publish_at` date DEFAULT NULL,
  `unpublish_at` date DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categorys_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorys`
--

INSERT INTO `categorys` (`id`, `user_id`, `parent_id`, `name`, `is_show_title`, `link`, `slug`, `type`, `blog`, `status`, `language`, `images`, `order`, `body`, `publish_at`, `unpublish_at`, `deleted`, `created_at`, `updated_at`) VALUES
(3, 11, 0, 'Slide', 1, NULL, 'slide', NULL, NULL, 1, NULL, NULL, 0.01, NULL, '2018-12-02', '2018-12-02', 1, '2018-12-07 20:12:48', '2019-01-09 01:29:07'),
(4, 11, 0, 'Video', 1, 'qwew', 'video', NULL, NULL, 1, NULL, 'http://localhost/cms/storage/app/public/images/Dec-2018/077c22447c33a737daeaa5acba0f8fa9.jpg', 3.01, NULL, NULL, NULL, 1, '2018-12-07 20:12:55', '2019-01-09 01:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `emps`
--

DROP TABLE IF EXISTS `emps`;
CREATE TABLE IF NOT EXISTS `emps` (
  `firstname` varchar(220) NOT NULL,
  `lastname` varchar(220) NOT NULL,
  `designation` varchar(220) NOT NULL,
  `salary` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emps`
--

INSERT INTO `emps` (`firstname`, `lastname`, `designation`, `salary`) VALUES
('Tom', 'Cruise', 'MD', 500000),
('Tyler', 'Horne', 'CEO', 250000);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `zipcode`, `code`, `image`, `status`, `created_at`, `updated_at`) VALUES
(4, 'English', '63', 'en', NULL, 1, '2019-01-09 02:01:44', '2019-01-09 02:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `madia_images`
--

DROP TABLE IF EXISTS `madia_images`;
CREATE TABLE IF NOT EXISTS `madia_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `meta_option` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `menu_type_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` double(8,2) DEFAULT NULL,
  `publish_at` date DEFAULT NULL,
  `unpublish_at` date DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lavel` int(11) DEFAULT NULL,
  `is_show_title` int(11) NOT NULL,
  `deleted` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_user_id_index` (`user_id`),
  KEY `menus_menu_type_id_index` (`menu_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `user_id`, `menu_type_id`, `parent_id`, `name`, `link`, `slug`, `type`, `status`, `language`, `description`, `images`, `order`, `publish_at`, `unpublish_at`, `color`, `icon_class`, `target`, `lavel`, `is_show_title`, `deleted`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 11, 1, 0, 'Home', NULL, 'home', NULL, 1, NULL, '<p id=\"des\"></p>', NULL, 1.00, NULL, NULL, NULL, NULL, '_self', 0, 1, 1, NULL, '2018-12-07 01:12:31', '2018-12-07 01:04:31'),
(2, 11, 2, 0, 'Tour', NULL, 'tour', NULL, 1, 4, '<p id=\"des\"></p>', NULL, 2.00, NULL, NULL, NULL, NULL, '_self', 0, 1, 1, NULL, '2018-12-07 02:12:31', '2019-01-09 20:00:49'),
(3, 11, 2, 0, 'Trip', NULL, 'trip', NULL, 1, 4, '<p id=\"des\"></p>', NULL, 3.00, NULL, NULL, NULL, NULL, '_self', 0, 1, 1, NULL, '2019-02-26 19:02:39', '2019-02-26 19:38:39');

-- --------------------------------------------------------

--
-- Table structure for table `menu_pages`
--

DROP TABLE IF EXISTS `menu_pages`;
CREATE TABLE IF NOT EXISTS `menu_pages` (
  `menu_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_types`
--

DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE IF NOT EXISTS `menu_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_types`
--

INSERT INTO `menu_types` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(2, 'Main Menu', '<p>main menues</p>', '2018-12-07 20:04:57', '2019-01-09 19:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `methods`
--

DROP TABLE IF EXISTS `methods`;
CREATE TABLE IF NOT EXISTS `methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `methods`
--

INSERT INTO `methods` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'user', 'User', '2018-11-30 20:12:16', '2018-12-06 01:12:54'),
(5, 'method', 'Method', '2018-12-06 19:12:06', NULL),
(6, 'permission', 'Permission', '2018-12-06 19:12:39', NULL),
(7, 'role', 'Role', '2018-12-06 19:12:47', NULL),
(8, 'menu', 'Menu', '2018-12-06 19:12:00', NULL),
(9, 'menu_type', 'Menu Type', '2018-12-06 19:12:13', NULL),
(10, 'post', 'Post', '2018-12-10 20:12:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_29_061054_create_methods_table', 1),
(4, '2018_11_29_061147_create_permissions_table', 1),
(5, '2018_11_29_061218_create_roles_table', 1),
(6, '2018_11_29_061334_create_role_permissions_table', 1),
(7, '2018_11_29_061419_create_user_rolse_table', 1),
(8, '2018_11_30_042543_create_menu_types_table', 1),
(9, '2018_11_30_042617_create_madia_images_table', 1),
(10, '2018_11_30_042638_create_page_categorys_table', 1),
(11, '2018_11_30_042714_create_menus_table', 1),
(12, '2018_11_30_042743_create_posts_table', 1),
(13, '2018_11_30_042834_create_categorys_table', 1),
(14, '2018_11_30_043435_create_menu_pages_table', 1),
(15, '2018_11_30_043639_create_post_tags_table', 1),
(16, '2018_11_30_043833_create_page_templates_table', 1),
(17, '2018_11_30_044114_create_templates_table', 1),
(18, '2018_11_30_044657_create_social_medias_table', 1),
(19, '2018_12_04_040847_create_settings_table', 1),
(20, '2018_12_04_042045_create_translations_table', 1),
(21, '2018_12_05_051106_create_languages_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(220) NOT NULL,
  `position` varchar(220) NOT NULL,
  `module_type` varchar(220) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_categorys`
--

DROP TABLE IF EXISTS `page_categorys`;
CREATE TABLE IF NOT EXISTS `page_categorys` (
  `page_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_templates`
--

DROP TABLE IF EXISTS `page_templates`;
CREATE TABLE IF NOT EXISTS `page_templates` (
  `tag_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `method_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `method_id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 1, 'user_create', 'User Create', '2018-12-02 23:12:40', NULL),
(2, 1, 'user_edit', 'User Edit', '2018-12-02 23:12:57', NULL),
(3, 1, 'user_delete', 'User Delete', '2018-12-02 23:12:31', NULL),
(11, 1, 'user_view', 'View', '2018-12-06 19:12:01', NULL),
(12, 1, 'user_show', 'Show', '2018-12-06 19:12:26', NULL),
(13, 5, 'method_create', 'Create', '2018-12-06 19:12:56', NULL),
(14, 5, 'method_view', 'View', '2018-12-06 19:12:31', NULL),
(15, 5, 'method_delete', 'Delete', '2018-12-06 19:12:57', NULL),
(16, 5, 'method_edit', 'Edit', '2018-12-06 19:12:14', NULL),
(17, 6, 'permission_view', 'View', '2018-12-06 19:12:38', NULL),
(18, 6, 'permission_create', 'Create', '2018-12-06 19:12:51', NULL),
(19, 6, 'permission_edit', 'Edit', '2018-12-06 19:12:21', NULL),
(20, 6, 'permission_delete', 'Delete', '2018-12-06 19:12:39', NULL),
(21, 7, 'role_view', 'View', '2018-12-06 19:12:57', NULL),
(22, 7, 'role_create', 'Create', '2018-12-06 19:12:11', NULL),
(23, 7, 'role_edit', 'Edit', '2018-12-06 19:12:34', NULL),
(24, 7, 'role_delete', 'Delete', '2018-12-06 19:12:05', NULL),
(25, 8, 'menu_view', 'View', '2018-12-06 19:12:48', NULL),
(26, 8, 'menu_create', 'Create', '2018-12-06 19:12:10', NULL),
(27, 8, 'menu_edit', 'Edit', '2018-12-06 19:12:24', NULL),
(28, 8, 'menu_delete', 'Delete', '2018-12-06 19:12:42', NULL),
(29, 9, 'menu_type_view', 'View', '2018-12-06 19:12:04', '2018-12-06 19:12:59'),
(30, 9, 'menu_type_create', 'Create', '2018-12-06 19:12:18', '2018-12-06 19:12:38'),
(31, 9, 'menu_type_edit', 'Edit', '2018-12-06 19:12:39', NULL),
(32, 9, 'menu_type_delete', 'Delete', '2018-12-06 19:12:08', NULL),
(33, 10, 'post_delete', 'Delete', '2018-12-10 20:12:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `is_show_title` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_body` text COLLATE utf8mb4_unicode_ci,
  `seo_keyword` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `language` int(11) DEFAULT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` double(8,2) DEFAULT NULL,
  `short_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `body_info` text COLLATE utf8mb4_unicode_ci,
  `publish_at` date DEFAULT NULL,
  `unpublish_at` date DEFAULT NULL,
  `featured` int(11) DEFAULT NULL,
  `count_page` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `parent_id`, `category_id`, `is_show_title`, `name`, `seo_name`, `seo_body`, `seo_keyword`, `link`, `slug`, `type`, `status`, `language`, `images`, `order`, `short_body`, `body`, `body_info`, `publish_at`, `unpublish_at`, `featured`, `count_page`, `deleted`, `deleted_at`, `created_at`, `updated_at`) VALUES
(6, 11, 3, NULL, 1, 'test3', NULL, NULL, NULL, NULL, 'test3', NULL, 1, NULL, 'http://localhost/cms/storage/app/public/images/Jan-2019/1c081fc4e5bc44261d9c41592f6889a0.jpg', 0.01, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2018-12-10 22:12:53', '2019-01-03 23:58:46'),
(7, 11, 3, NULL, 1, 'ADMIN TEST', NULL, NULL, NULL, NULL, 'admin-test', NULL, 1, NULL, NULL, 7.00, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2018-12-10 22:12:36', '2018-12-10 22:42:36'),
(8, 11, 3, NULL, 1, 'user', NULL, NULL, NULL, 'photo-gallery', 'user', NULL, 1, NULL, NULL, 8.00, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-01-03 20:01:11', '2019-01-03 20:17:11'),
(9, 0, NULL, NULL, 0, 'myVideoEdit', NULL, NULL, NULL, 'aJOTlE1K90k', NULL, 'video', 1, NULL, 'http://localhost/touring/storage/app/public/images/Jan-2019/Lotus Publish Promote.mp4', NULL, '', '<p id=\"des\">rererer<p id=\"des\"></p><p id=\"des\"></p><p></p></p>', NULL, '1970-01-01', '1970-01-01', NULL, NULL, NULL, NULL, '2019-01-08 20:01:48', '2019-01-10 18:49:17'),
(15, 0, NULL, 3, 0, 'myVidEdit', NULL, NULL, NULL, 'aJOTlE1K90k', NULL, 'video', 1, NULL, 'http://localhost/touring/storage/app/public/images/Jan-2019/Lotus Tour Promote (2).mp4', NULL, '', 'trrdtt<p id=\"des\"><p id=\"des\"></p></p>', NULL, '2019-01-04', '2019-01-17', NULL, NULL, NULL, NULL, '2019-01-08 21:01:00', '2019-01-10 19:08:02'),
(16, 0, NULL, 3, 0, 'mySlide', NULL, NULL, NULL, 'aJOTlE1K90k', NULL, 'slide', 1, NULL, 'http://localhost/touring/storage/app/public/images/Jan-2019/cover-img-4.jpg', NULL, '', '<p id=\"des\"><p id=\"des\">New Des Edit2</p><p id=\"des\"></p><p></p></p>', NULL, '2019-01-22', '2019-01-29', NULL, NULL, NULL, NULL, '2019-01-09 01:01:13', '2019-01-10 01:41:16'),
(17, 0, NULL, 3, 0, 'MySlide2', NULL, NULL, NULL, NULL, NULL, 'slide', 1, NULL, 'http://localhost/touring/storage/app/public/images/Jan-2019/hotel-3.jpg', NULL, '', 'Are u find the Nature?<p id=\"des\"></p>', NULL, '2019-01-11', '2019-01-22', NULL, NULL, NULL, NULL, '2019-01-09 01:01:53', '2019-01-10 01:18:46'),
(18, 0, NULL, 3, 0, 'NewSlide', NULL, NULL, NULL, NULL, NULL, 'slide', 1, 4, 'http://localhost/touring/storage/app/public/images/Jan-2019/img_bg_2.jpg', NULL, '', 'Come here to get Relax..<p id=\"des\"></p>', NULL, '1970-01-01', '1970-01-01', NULL, NULL, NULL, NULL, '2019-01-09 03:01:11', '2019-01-10 01:19:02'),
(19, 11, 0, 3, 1, 'New Post', NULL, NULL, NULL, 'llsldf3341', 'new-post', 'post', 1, 4, 'http://localhost/touring/storage/app/public/images/Jan-2019/cover-img-3.jpg', 0.01, '', '<p id=\"des\"></p><p id=\"des\">PostEdit2</p><p></p>', NULL, '2019-01-04', '2019-01-04', NULL, NULL, 1, NULL, '2019-01-10 01:01:18', '2019-01-10 01:42:06'),
(20, 11, 0, 0, 1, 'test', NULL, NULL, NULL, 'http://localhost/cms/public/test', 'test', 'post', 1, 4, NULL, 20.00, '', 'tet<p id=\"des\"></p>', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-09-20 01:09:38', '2019-09-20 01:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

DROP TABLE IF EXISTS `post_tags`;
CREATE TABLE IF NOT EXISTS `post_tags` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE IF NOT EXISTS `role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL,
  KEY `role_permissions_role_id_index` (`role_id`),
  KEY `role_permissions_permission_index` (`permission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`role_id`, `permission`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(2, 1),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 17),
(2, 18),
(2, 21),
(2, 22),
(2, 25),
(2, 26),
(2, 29),
(2, 30);

-- --------------------------------------------------------

--
-- Table structure for table `rolse`
--

DROP TABLE IF EXISTS `rolse`;
CREATE TABLE IF NOT EXISTS `rolse` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rolse`
--

INSERT INTO `rolse` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '2018-11-30 20:12:32', '2018-12-06 21:12:18'),
(2, 'user', 'User', '2018-12-02 19:12:09', '2018-12-06 21:12:09');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `settings_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting_group`
--

DROP TABLE IF EXISTS `setting_group`;
CREATE TABLE IF NOT EXISTS `setting_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_id` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `social_medias`
--

DROP TABLE IF EXISTS `social_medias`;
CREATE TABLE IF NOT EXISTS `social_medias` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_medias`
--

INSERT INTO `social_medias` (`id`, `name`, `images`, `class_icon`, `body`, `status`, `created_at`, `updated_at`) VALUES
(16, 'test', 'http://localhost/touring/storage/app/public/images/Jan-2019/blog-5.jpg', 'fab fa-app-store-ios', '', 1, NULL, '2019-01-09 23:22:47'),
(17, 'userEdit', 'http://localhost/touring/storage/app/public/images/Jan-2019/cover-img-1.jpg', 'sdf', '', 1, NULL, '2019-01-09 23:23:17');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lavel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(11) NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usercode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `language` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `supspand` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_usercode_unique` (`usercode`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `usercode`, `full_name`, `email`, `email_verified_at`, `password`, `phone`, `address`, `language`, `image`, `last_login`, `last_logout`, `status`, `supspand`, `deleted`, `remember_token`, `created_at`, `updated_at`) VALUES
(6, 'knaa', 'eee', 'knee', 'US6', 'knaa eee', 'knee@gmail.com', NULL, '$2y$10$X.Jvek0HwjjiKNjuyklnmeJPnc5WKQUM8HAIynzLHG8kJ3dOJZXlO', '123', NULL, NULL, '66a650d2cf515b3d71c42388388bc28d.jpg', NULL, NULL, 1, NULL, NULL, 'iLwzB8MJ65vp1UY2xVAV1XaZFhK5bBB7tcGC76d4FdHE1boll5g6kdTl3Sm8', NULL, '2018-12-06 00:25:20'),
(7, 'sdf', 'sdf', 'ad', 'US7', 'sdfsdf', 'assd@gmail.com', NULL, '$2y$10$bBgrK8Z6mSD2P/oDbcRcz.OOgfDcBkMnDa3o/JuGN/pO/8pFKAdgm', NULL, NULL, NULL, '170303_onte_mck_uawm_(1).jpg', NULL, NULL, 1, NULL, NULL, 'tHSEUhsFjsSMPrguByM5Ddy4sGa920f1lGjktnT7TMa9dIb3JxigfNRBYm2L', NULL, '2018-12-05 19:40:12'),
(9, 'cms', 'cms', 'cms', 'US9', 'cmscms', 'cms@gmail.com', NULL, '$2y$10$U/Op6wSXUJ61SEAYSoZJXeW44RRUpoXdRYroaimWpEBfBD0lSRdxC', '0342', NULL, NULL, 'avatar-3.jpg', NULL, NULL, 1, NULL, NULL, 'qDmPlsKJsBwWv3pWcCCP5bNxW10sCmBhg4GcONwDUc6gcXusWuapSAZgtncR', NULL, '2018-12-05 00:13:33'),
(11, 'admin', 'admin', 'admin', 'US11', 'admin admin', 'admin@gmail.com', NULL, '$2y$10$bAhuogXfZlkRFOI2Td.kjOuKLZfu0pvNPwS9vCc0WEDZZOEbP6A/u', '012333123', NULL, NULL, '', NULL, NULL, 1, NULL, NULL, 'w7U0JEe2dhQXMSxFzOlNvSLB4F385PG4GCuCP2sx91xIUlE67GyFnA54eG0k', NULL, '2019-01-29 01:42:18'),
(12, 'kaka', 'tea', 'developer', 'US12', 'kaka tea', 'developer@gmail.com', NULL, '$2y$10$C09ILmhZ28WhTvwsoUNgMuIK3AjkuwDUMYKayQqKpWDZRMwNGV44u', NULL, NULL, NULL, '170303_onte_mck_uawm_(1).jpg', NULL, NULL, 1, NULL, NULL, NULL, NULL, '2018-12-06 00:23:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(5, 1),
(6, 1),
(8, 2),
(9, 2),
(10, 1),
(12, 1),
(11, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
